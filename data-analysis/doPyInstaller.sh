#!/bin/bash
rm -R build/ dist/ audiostellar-data-analysis.spec
pyinstaller --onefile --name audiostellar-data-analysis --additional-hooks-dir=. doProcess.py && 
cp dist/audiostellar-data-analysis ../bin/ &&
printf "\n\nBinary has been built and copied to ../bin/ successfully.\n\n"
