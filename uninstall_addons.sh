#!/bin/sh
PROJECT_DIR=$(dirname "$0")

if [ -z "${OF_ROOT}" ]; then
    echo "OF_ROOT enviroment variable is undefined."

    read -p "Is your app in apps/myApps folder? " -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        OF_ROOT=$PROJECT_DIR/../../..
    else
        echo "Rerun this script setting OF_ROOT enviroment variable: $ export OF_ROOT=<path/to/of>\n "
        exit
    fi
fi

echo "Removing assets"
rm $PROJECT_DIR/bin/data/assets
echo "Removing ofxMidi"
rm -rf $OF_ROOT/addons/ofxMidi
echo "Removing ofxSimpleTimer"
rm -rf $OF_ROOT/addons/ofxSimpleTimer
echo "Removing ofxJSON"
rm -rf $OF_ROOT/addons/ofxJSON
echo "Removing ofxConvexHull"
rm -rf $OF_ROOT/addons/ofxConvexHull
echo "Removing ofxTweener"
rm -rf $OF_ROOT/addons/ofxTweener
echo "Removing ofxImGui"
rm -rf $OF_ROOT/addons/ofxImGui
echo "Removing ofxAudioFile"
rm -rf $OF_ROOT/addons/ofxAudioFile
echo "Removing ofxPDSP"
rm -rf $OF_ROOT/addons/ofxPDSP

echo "Done."
