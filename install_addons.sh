#!/bin/bash
PROJECT_DIR=$(dirname "$0")

if [ -z "${OF_ROOT}" ]; then
    echo "OF_ROOT enviroment variable is undefined."

    read -p "Is your app in apps/myApps folder? [y/n] " -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        OF_ROOT=$PROJECT_DIR/../../..
    else
        echo "Rerun this script setting OF_ROOT enviroment variable: $ export OF_ROOT=<path/to/of>\n "
        exit
    fi
fi

mkdir -p $PROJECT_DIR/bin/data 
ln -s ../../assets/ $PROJECT_DIR/bin/data/assets

git clone https://github.com/danomatika/ofxMidi $OF_ROOT/addons/ofxMidi
git clone https://github.com/HeliosInteractive/ofxSimpleTimer $OF_ROOT/addons/ofxSimpleTimer
git clone https://github.com/jeffcrouse/ofxJSON $OF_ROOT/addons/ofxJSON
git clone https://github.com/genekogan/ofxConvexHull $OF_ROOT/addons/ofxConvexHull
git clone https://github.com/larsberg/ofxTweener $OF_ROOT/addons/ofxTweener
git clone https://github.com/jvcleave/ofxImGui $OF_ROOT/addons/ofxImGui
git clone https://github.com/npisanti/ofxAudioFile $OF_ROOT/addons/ofxAudioFile
git clone https://github.com/macramole/ofxPDSP $OF_ROOT/addons/ofxPDSP

echo "DONE"
