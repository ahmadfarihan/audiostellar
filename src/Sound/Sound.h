#pragma once

#include "ofMain.h"
#include "Utils.h"
#include "ColorPaletteGenerator.h"
#include "Tweener.h"
#include "Tooltip.h"

#define SIZE_ORIGINAL 2

#define TSV_FIELD_X 0
#define TSV_FIELD_Y 1
#define TSV_FIELD_Z 2
#define TSV_FIELD_CLUSTER 3
#define TSV_FIELD_NAME 4

class Voice;

class Sound {

private:
    string name = "";
    bool hide = false;
    int cluster = -1;

    float sizeInc = 0.2; //animación de envolvente de sonido
    ofVec2f originalPosition;

    bool finishedPlaying = true;

    TWEEN::Manager tweenManager;
    shared_ptr<TWEEN::Tween> currentTween = NULL;
    int glowOpacity = 0;

    void startGlow();
    void stopGlow();

    const int OPACITY_MIN = 100; //60;
    float colorOpacity = 0;
    
public:
    Sound (string _name, ofVec3f _position,int _cluster);
    Sound (string _name);

    int id;
    ofVec3f position;
    float size = SIZE_ORIGINAL;

    bool hovered = false;
    bool clusterIsHovered = true;
    bool selected = false;
    static ofColor colorSelected;

    void setHovered(bool b);
    bool getHovered();

    static ofImage imgGlow;

    void drawGlow();
    void draw();
    void update();
    // void mouseMoved(int x, int y);

    bool onSound(ofVec2f playerPosition);
    string getFileName();
    ofVec2f getPosition();
    void setCluster(int cluster);
    int getCluster();
    bool getHide();
    void setHide(bool h);
    void toggleHide();

    void useOriginalPosition();
    
    string fileName = "";
    string hoveredSoundFolderPath = "";
    void drawGui();
    
    int channel0 = 0;
    int channel1 = 1;
    
    int selectedOutput = 0;
    
    float volume = 1.0;
    
    bool mute = false;
    
    Voice * play();
};
