#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"
#include "ofxJSON.h"

class Voices;

class AudioEngine {
    
private:
    
    AudioEngine();
    static AudioEngine * instance;

    vector <ofSoundDevice> devices;
    
public:
    
    static AudioEngine * getInstance();
    
    pdsp::Engine engine;
    
    void setup(int deviceID, int sampleRate, int bufferSize);
	void stop();

    int getDefaultAudioOutDeviceIndex();
    int getAudioOutNumChannels(int deviceID);
    
    void selectAudioOutDevice(int deviceID);
    void selectAudioOutDevice(string deviceName);
    void selectSampleRate(int sampleRate);
    void selectBufferSize(int bufferSize);

    vector <ofSoundDevice> listDevices();
    void refreshDeviceList();
    
    int selectedAudioOutDeviceID;
    int selectedAudioOutNumChannels;
    
    struct Output {
        string type;
        int channel0;
        int channel1;
        bool enabled;
        bool selected;
        string label;
    };
    
    vector <Output> outputs;
    
    const vector<int> bufferSizes {32, 64, 128, 256, 512, 1024, 2048, 4096};
    
    int selectedSampleRate = 44100;
    int selectedBufferSize = 512;
    
    void createOutputs();
    
    Json::Value save();
    void load(Json::Value json);
    
    void exit();
    
};
