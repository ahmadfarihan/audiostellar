#include "Sounds.h"
#include "Voices.h"

Sounds* Sounds::instance = nullptr;

Sounds::Sounds() {

    this->voices = Voices::getInstance();

    volumen = 0.5f;
    replaySound = false;
    useOriginalPositions = false;
    epsDbScan = 15;

    windowOriginalWidth = ofGetWidth();
    windowOriginalHeight = ofGetHeight();
    ofAddListener(OscServer::oscEvent, this, &Sounds::oscListener);
    //ofAddListener(OscServer::oscEvent, this,
                  //&TidalOscDispatcher::onOscMsg);

    font.load( OF_TTF_MONO, 10 );

}

Sounds* Sounds::getInstance(){
    if( instance == nullptr){
       instance = new Sounds();
    }

    return instance;
}

void Sounds::loadSounds(ofxJSONElement jsonFile){

    if(!sounds.empty()){
        reset();
    }

    string soundsDir = jsonFile["audioFilesPathAbsolute"].asString();

    vector<string> soundFiles = ofSplitString(jsonFile["tsv"].asString(), "|");

    initialWindowHeight = ofGetHeight();
    initialWindowWidth = ofGetWidth();
    setSpaceLimits(soundFiles);

    for(int i = 0; i<soundFiles.size() - 1; i++) {
        if(soundFiles[i] == "")continue;
        vector<string>lineElements = ofSplitString(soundFiles[i],",");

        int cluster = ofToInt(lineElements[TSV_FIELD_CLUSTER]);
        string name = lineElements[TSV_FIELD_NAME];

        ofVec3f position = soundToCamCoordinates( ofVec3f(
                    ofToFloat(lineElements[TSV_FIELD_X]),
                    ofToFloat(lineElements[TSV_FIELD_Y]),
                    ofToFloat(lineElements[TSV_FIELD_Z])) );

        string file = soundsDir + name;

        Sound *sound = new Sound(file, position, cluster);
        sound->id = idCount;
        idCount++;

        sounds.push_back(sound);
    }
    // Esto antes no estaba comentado pero
    //doClustering();
}

void Sounds::reset() {
    //Resetea el tamaño

    AudioEngine::getInstance()->engine.stop();

    sounds.clear(); // revisar esto, se esta liberando bien ?
    clusterNames.clear();
    idCount = 0;
    backgroundAlpha = BG_ALPHA_MIN;

    windowOriginalWidth = ofGetWidth();
    windowOriginalHeight = ofGetHeight();

    voices->reset();

    AudioEngine::getInstance()->engine.start();
}

void Sounds::update() {
    for (int i = 0; i < sounds.size(); i++) {
        sounds[i]->update();
    }

    if(OscServer::enable) {
        oscOpacity-=2;
    }

    animateClusterNames();
}

void Sounds::draw() {
    ofFill();

    drawBackground();

    ofSetColor(255,255,255,255);

    if (clusterNamesOpacity > 0) {

        ofSetColor(ofColor::white, clusterNamesOpacity);

        for ( int i = 0 ; i < hulls.size() ; i++ ) {
            ImVec2 textSize = ImGui::CalcTextSize(clusterNames[i].c_str(), NULL, true);
            font.drawString(clusterNames[i], hulls[i].getCentroid2D().x - (textSize.x / 2), hulls[i].getCentroid2D().y);
        }
    }



    //ofSetCircleResolution(100);
    //ofDrawEllipse(hulls[selectedCluster].getCentroid2D(), 20,20);
    for (int i = 0; i < sounds.size(); i++) {
        if(!sounds[i]->getHide()) {
            sounds[i]->draw();
        }

        if ( showSoundFilenamesTooltip && sounds[i]->getHovered() ) {
            sounds[i]->drawGui();
        }
    }
    if(OscServer::enable) {
        oscDraw();
    }
}

void Sounds::drawBackground()
{
    ofPushMatrix();
    ofTranslate( -windowOriginalWidth*BG_TRANSLATION, -windowOriginalHeight*BG_TRANSLATION );
    ofSetColor(255,255,255,round(backgroundAlpha));
    fboBackground.draw(0,0);

    backgroundAlpha += BG_ALPHA_STEP * backgroundDirection * ofGetLastFrameTime() * 40;

    if ( backgroundAlpha >= BG_ALPHA_MAX ) {
        backgroundDirection = -1;
    } else if ( backgroundAlpha <= BG_ALPHA_MIN ) {
        backgroundDirection = 1;
    }
    ofPopMatrix();
}

void Sounds::generateBackground()
{
    fboBackground.allocate(windowOriginalWidth * 1.5, windowOriginalHeight * 1.5, GL_RGBA);

    fboBackground.begin();
    ofClear(25,0);
    ofPushMatrix();
    ofTranslate( windowOriginalWidth * BG_TRANSLATION, windowOriginalHeight * BG_TRANSLATION );

    float glowSize = SIZE_ORIGINAL * BG_GLOW_SIZE;

    for ( auto * sound : sounds ) {
        if ( sound->getCluster() < 0 ) {
            continue;
        }
        ofSetColor( ColorPaletteGenerator::getColor(sound->getCluster()) );
        //            ofDrawCircle(position.x, position.y, glowBigSize);
        Sound::imgGlow.draw(sound->position.x - glowSize / 2,
                            sound->position.y - glowSize / 2,
                            glowSize,
                            glowSize);
    }

    ofNoFill();
    ofSetColor(255,0,0);
    ofPopMatrix();
    //       ofDrawRectangle(1,1,fboBackground.getWidth()-2, fboBackground.getHeight()-1);
    fboBackground.end();
}

void Sounds::animateClusterNames() {
    if (showClusterNames){
        if (clusterNamesOpacity < 255){
            clusterNamesOpacity += clusterNamesSpeed;
        }
    } else {
        if (clusterNamesOpacity > 0){
            clusterNamesOpacity -= clusterNamesSpeed;
        }
    }
}

void Sounds::findConvexHulls() {
    ofxConvexHull convexHull;
    vector<ofVec3f> cluster;
    string strDebug = "clusterIDS: ";

    hulls.clear();

    for ( auto clusterId : clusterIds ) {
        strDebug += ofToString(clusterId) + ",";
        cluster.clear();
        for ( auto * sound : sounds ) {
            if ( sound->getCluster() == clusterId ) {
                cluster.push_back( sound->getPosition() );
            }
        }

        ofPolyline polyHull;
        if ( cluster.size() >= 3 ) {
            vector<ofPoint> hull = convexHull.getConvexHull( cluster );

            for ( int i = 0 ; i < hull.size() ; i++ ) {
                polyHull.addVertex( hull[i] );
            }
            polyHull.close();

            //agrando desde el centro
            for( int i = 0 ; i < polyHull.size() ; i++ ) {
                auto &p = polyHull.getVertices()[i];

                p.x += polyHull.getNormalAtIndex(i).x * 15;
                p.y += polyHull.getNormalAtIndex(i).y * 15;
            }
        }

        hulls.push_back( polyHull );
    }
}

void Sounds::updateSoundPosition(Sound * sound) {
    soundPosition * found = nullptr;
    for ( int i = 0 ; i < soundPositions.size() ; i++ ) {
        if ( soundPositions[i].sound == sound ) {
            found = &soundPositions[i];
            break;
        }
    }

    if ( found == nullptr ) {
        found = new soundPosition;
        found->sound = sound;

        soundPositions.push_back(*found);
    }

    found->position.x = sound->position.x;
    found->position.y = sound->position.y;
}

void Sounds::mouseMoved(int x, int y, bool onGui) {
    if ( onGui ) {
        for ( auto * sound : sounds ) {
            sound->clusterIsHovered = true;
        }
        return;
    }

    if ( hoveredActivated ) {
        Sound * nearestSound = getNearestSound( ofVec2f(x,y) );
        allSoundsHoveredOff(nearestSound);
        if (nearestSound != NULL) {
            if ( !nearestSound->getHovered() ) {
                nearestSound->setHovered(true);
            }
        }
    }

    set<int> selectedClusters;
    selectedCluster = -1;
    for ( int i = 0 ; i < hulls.size() ; i++ ) {
        if ( hulls[i].size() >= 3 && hulls[i].inside( x, y) ) {
            selectedClusters.insert(i);
        }
    }

    for ( auto cluster : selectedClusters ) {
        if ( selectedCluster == -1 ) {
            selectedCluster = cluster;
        } else {
            if ( hulls[cluster].getArea() <
                 hulls[selectedCluster].getArea() ) {
                selectedCluster = cluster;
            }
        }
    }

    hoveredClusterID = selectedCluster;

    for ( auto * sound : sounds ) {
        if ( (selectedCluster == -1 && !showClusterNames) || sound->getCluster() == selectedCluster ) {
            sound->clusterIsHovered = true;
        } else {
            sound->clusterIsHovered = false;
        }
    }

    //    string strDebug = "selectedClusters: ";
    //    for ( auto cluster : selectedClusters ) {
    //        strDebug += ofToString(cluster) + ",";
    //    }
    //    ofLog() << strDebug;
    //    ofLog() << "selectedCluster: " << selectedCluster;
}

void Sounds::mouseDragged(ofVec2f p, int button) {
    if ( !useOriginalPositions && button == 2 ) {
        Sound * hoveredSound = getHoveredSound();

        if ( hoveredSound != NULL ) {
            hoveredSound->position = p;
            updateSoundPosition(hoveredSound);
            soundIsBeingMoved = true;
        }
    }

    if ( button == 0 ) {
        Sound * nearestSound = getNearestSound(p);

        if ( nearestSound != NULL &&
             nearestSound != lastDraggedSound ) {
            playSound( nearestSound );
        }

        lastDraggedSound = nearestSound;
    }
}

void Sounds::mousePressed(int x, int y, int button){

}

void Sounds::mouseReleased(int x, int y, int button)
{
    soundIsBeingMoved = false;
}


void Sounds::allSoundsSelectedOff() {
    for(int i = 0; i < sounds.size(); i++) {
        sounds[i]->selected = false;
    }
}
void Sounds::allSoundsHoveredOff(Sound * except) {
    for(int i = 0; i < sounds.size(); i++) {
        if ( except != NULL && sounds[i] == except ) {
            continue;
        }
        if ( sounds[i]->getHovered() ) {
            sounds[i]->setHovered(false);
        }
    }
}

Sound * Sounds::playSoundAt(ofVec2f playerPosition) {
    Sound * nearestSound = getNearestSound(playerPosition);

    if ( nearestSound != NULL ) {
        playSound( nearestSound );
    }

    return nearestSound;
}

Sound * Sounds::getNearestSound(ofVec2f position) {
    if ( sounds.size() == 0 ) {
        return NULL;
    }
    float minDistance = sounds[0]->getPosition().squareDistance( position );
    int minDistanceIndex = -1;

    float currentDistance = 0;

    for(int i = 1; i < sounds.size(); i++) {
        if ( sounds[i]->getHide() ) {
            continue;
        }

        currentDistance = sounds[i]->getPosition().squareDistance( position );

        if ( currentDistance > pow(sounds[i]->size * 3,2) ) {
            continue;
        }

        if ( currentDistance < minDistance ) {
            minDistance = currentDistance;
            minDistanceIndex = i;
        }
    }

    if ( minDistanceIndex != -1 ) {
        return sounds[minDistanceIndex];
    } else {
        return NULL;
    }
}

Sound * Sounds::getHoveredSound() {
    for(int i = 0; i < sounds.size(); i++) {
        if ( sounds[i]->hovered ) {
            return sounds[i];
        }
    }

    return NULL;
}

void Sounds::playSound(Sound * sound, float volumeMult) {
    voices->playSound(sound, volumeMult);
    lastPlayedSound = sound;
    
    if (oscGetPlayedSoundIDEnabled) {
        OscServer::sendMessage( OSC_GET_PLAYED_SOUND_ID, sound->id );
    }
}

void Sounds::playSound(int id, float volumeMult){
    playSound( getSoundById(id), volumeMult );
}

void Sounds::setSpaceLimits(vector<string> soundsCoords) {
    ofVec3f max(-99999999, -99999999, -99999999);
    ofVec3f min(99999999, 99999999, 99999999);
    // spaceLimits = {ofVec2f(0,0),ofVec2f(1500,1500)};
    spaceLimits[0] = max;
    spaceLimits[1] = min;


    for(int i = 0; i< soundsCoords.size() - 1; i++) {
        if(soundsCoords[i] == "")continue;
        auto lineElements = ofSplitString(soundsCoords[i],",");

        ofVec3f position(ofToFloat(lineElements[TSV_FIELD_X]), ofToFloat(lineElements[TSV_FIELD_Y]), ofToFloat(lineElements[TSV_FIELD_Z]));

        spaceLimits[0].x = Utils::getMaxVal(position.x, spaceLimits[0].x);
        spaceLimits[0].y = Utils::getMaxVal(position.y, spaceLimits[0].y);
        spaceLimits[0].z = Utils::getMaxVal(position.z, spaceLimits[0].z);

        spaceLimits[1].x = Utils::getMinVal(position.x, spaceLimits[1].x);
        spaceLimits[1].y = Utils::getMinVal(position.y, spaceLimits[1].y);
        spaceLimits[1].z = Utils::getMinVal(position.z, spaceLimits[1].z);

    }

}

void Sounds::drawGui(){
    if(ImGui::Checkbox("Original Positions", &useOriginalPositions)){
            setUseOriginalPositions();
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SOUNDS_ORIGINAL_POSITIONS);
}

void Sounds::setClusteringOptionsScreenFlags(){
    haveToDrawClusteringOptionsScreen = true;
    isClusteringOptionsScreenOpen = true;
}

void Sounds::drawClusteringOptionsScreen(){
    if(haveToDrawClusteringOptionsScreen){
        if(isClusteringOptionsScreenOpen){

            ImGuiWindowFlags window_flags = 0;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

            if(ImGui::Begin("Clustering settings",
                            &isClusteringOptionsScreenOpen,
                            window_flags)){

                isClusteringOptionsScreenHovered = ImGui::IsWindowHovered(
                            ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                            | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                            | ImGuiHoveredFlags_ChildWindows);

                ImGui::Text("DBScan Settings");

                //Slider
                if(areAnyClusterNames()){
                    if(ImGui::Button("Delete cluster names and enable cluster options")){
                        deleteClusterNames();
                    }
                    ImGui::Text("");
                }else{
//                    ImGui::SliderInt("Eps", &epsDbScan, 5, 100);
                    if (ImGui::InputInt("Eps", &epsDbScan)) {
                        epsDbScan = ofClamp(epsDbScan, 5, 500);
                        doClustering();
                    }
                    if(ImGui::IsItemHovered())  Tooltip::setTooltip(Tooltip::CLUSTERS_EPS);

//                    ImGui::SliderInt("MinPts", &minPts, 3, 100);
                    if (ImGui::InputInt("MinPts", &minPts)) {
                        minPts = ofClamp(minPts, 3, 100);
                        doClustering();
                    }
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::CLUSTERS_MIN_PTS);
                }

                //Export files
                if(ImGui::Button("Export files")){
                   exportFiles();
                }
                if(ImGui::IsItemHovered()){
                    Tooltip::setTooltip(Tooltip::CLUSTERS_EXPORT_FILES);
                }
            }
            ImGui::End();
        }else{
                haveToDrawClusteringOptionsScreen = false;
                isClusteringOptionsScreenHovered = false;
        }
    }
}

void Sounds::setUseOriginalPositions() {
    for(int i = 0; i<soundPositions.size(); i++) {
        if (useOriginalPositions) {
            soundPositions[i].sound->useOriginalPosition();
        } else {
            soundPositions[i].sound->position = soundPositions[i].position;
        }
    }
}

void Sounds::onClusterToggle(int clusterIdx) {
    for(int i = 0; i<sounds.size(); i++) {
        if(sounds[i]->getCluster() == clusterIdx) {
            sounds[i]->toggleHide();
        }
    }
}

void Sounds::doClustering()
{
    DBScan dbscan( epsDbScan, minPts, getSoundsAsPoints());
    dbscan.run();
    setDBScanClusters(dbscan.points);
    findConvexHulls();
    generateBackground();
}

Sound * Sounds::getSoundByFilename(string filename) {
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( sounds[i]->getFileName() == filename ) {
            return sounds[i];
        }
    }
    return NULL;
}
Sound * Sounds::getSoundById(int id) {
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( sounds[i]->id == id ) {
            return sounds[i];
        }
    }
    return NULL;
}

vector<Sound *> Sounds::getSounds()
{
    return sounds;
}

vector<Sound *> Sounds::getSoundsByCluster(int clusterId)
{
    vector<Sound *> ret;
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( sounds[i]->getCluster() == clusterId ) {
            ret.push_back( sounds[i] );
        }
    }
    return ret;
}

vector<Sound *> Sounds::getSoundsByCluster(string name){
    vector<Sound *> snds;
    int clusterId = -1;

    for ( int i = 0 ; i < clusterNames.size(); i++){
        if (clusterNames[i] == name){
            clusterId = i;
            break;
        }
    }

    if ( clusterId != -1 ) {
        for ( int i = 0 ; i < sounds.size() ; i++ ) {
            if ( sounds[i]->getCluster() == clusterId ) {
                snds.push_back( sounds[i] );
            }
        }
    }

    return snds;

}

vector<Sound *> Sounds::getNeighbors(Sound *s, float threshold)
{
    ofVec2f position = s->getPosition();
    return getNeighbors(position, threshold);
}

vector<Sound *> Sounds::getNeighbors(ofVec2f position, float threshold)
{
    vector<Sound *> neighbors;

    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        ofVec2f p = sounds[i]->getPosition();
        if ( p != position && p.squareDistance( position ) <= threshold ) {
            neighbors.push_back(sounds[i]);
        }
    }

    return neighbors;
}

vector<ofPoint> Sounds::getSoundsAsPoints()
{
    vector<ofPoint> soundsAsPoints;

    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        soundsAsPoints.push_back( sounds[i]->getPosition() );
    }

    return soundsAsPoints;
}

void Sounds::setDBScanClusters(vector<dbscanPoint> points)
{
    clusterIds.clear();

    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        sounds[i]->setCluster( points[i].cluster );

        if ( points[i].cluster >= 0 ) {
            clusterIds.insert( points[i].cluster );
            //Para saber si cada cluster se está mostrando. Asumo que todos los clusters empiezan siendo mostrados...
            clustersShow.push_back(true);
        }
    }
    nClusters = clusterIds.size();
    for( int i = 0; i < nClusters; i++){
        clusterNames.push_back("");
    }
}


float Sounds::getVolume() {
    return volumen;
}

void Sounds::setFilenameLabel(string fileName){
    currentSoundLabel = fileName;
}

bool Sounds::getUseOriginalPositions(){
  return useOriginalPositions;
}

bool Sounds::getReplaySound(){
  return replaySound;
}

bool Sounds::getShowSoundFilenames(){
    return showSoundFilenamesTooltip;
}

ofVec3f Sounds::camToSoundCoordinates(ofVec2f camCoordinates) {
    return camToSoundCoordinates( ofVec3f(camCoordinates.x, camCoordinates.y, 0) );
}

ofVec3f Sounds::camToSoundCoordinates(ofVec3f camCoordinates)
{
    ofVec3f r;

    r.x = ofMap(camCoordinates.x,
                0 + spacePadding,
                initialWindowWidth - spacePadding,
                spaceLimits[1].x,
                spaceLimits[0].x);


    r.y = ofMap(camCoordinates.y,
                0 + spacePadding,
                initialWindowHeight - spacePadding,
                spaceLimits[1].y,
                spaceLimits[0].y);

    r.z = ofMap(camCoordinates.z,
                   0.5,1,
                   spaceLimits[1].z,
                   spaceLimits[0].z);

    return r;
}

ofVec3f Sounds::soundToCamCoordinates(ofVec3f soundCoordinates)
{
    ofVec3f r;

    r.x = ofMap(soundCoordinates.x,
                       spaceLimits[1].x,
                       spaceLimits[0].x,
                       0 + spacePadding,
                       initialWindowWidth - spacePadding);

    r.y = ofMap(soundCoordinates.y,
                       spaceLimits[1].y,
                       spaceLimits[0].y,
                       0 + spacePadding,
                       initialWindowHeight - spacePadding);

    r.z = ofMap(soundCoordinates.z,
                       spaceLimits[1].z,
                       spaceLimits[0].z,
                       0.5,1);

    return r;
}

void Sounds::setUseOrigPos(bool v){
  useOriginalPositions = v;
}

void Sounds::setReplaySound(bool v){
  replaySound = v;
}

void Sounds::setShowSoundFilenames(bool v){
  showSoundFilenamesTooltip = v;
}

Json::Value Sounds::save() {
    Json::Value root = Json::Value( Json::objectValue );
    Json::Value overridedPositions = Json::Value( Json::arrayValue );

    //positions
    for ( auto sPos : soundPositions ) {
        Json::Value soundPosition = Json::Value( Json::objectValue );
        soundPosition["id"] = sPos.sound->id;
        soundPosition["position"] = Json::Value( Json::arrayValue );
        soundPosition["position"].append( sPos.position.x );
        soundPosition["position"].append( sPos.position.y );

        overridedPositions.append(soundPosition);
    }

    //cluster names
    Json::Value jsonClusterNames = Json::Value(Json::arrayValue);
    for (auto clusterName : clusterNames ) {
        Json::Value jsonClusterName = Json::Value(Json::stringValue);
        jsonClusterName = clusterName;
        jsonClusterNames.append(jsonClusterName);
    }

    root["clusterNames"] = jsonClusterNames;
    root["overridedPositions"] = overridedPositions;
    root["DBScan_EPS"] = epsDbScan;
    root["DBScan_minPts"] = minPts;

    //muted sounds
    Json::Value muted = Json::Value(Json::arrayValue);
    for (auto sound : sounds) {
        if (sound->mute) {
            muted.append(sound->id);
        }
    }

    root["muted"] = muted;

    return root;
}

void Sounds::load( Json::Value jsonData ) {
    Json::Value overridedPositions = jsonData["overridedPositions"];
    for ( int i = 0 ; i < overridedPositions.size() ; i++ ) {
        // Sound * sound = getSoundById( overridedPositions[i]["id"] )
        soundPosition newSoundPosition;
        newSoundPosition.sound = getSoundById( overridedPositions[i]["id"].asInt() );

        if ( newSoundPosition.sound == NULL ) {
            ofLogWarning("Sound ID not found when loading session");
            continue;
        }

        newSoundPosition.position.x = overridedPositions[i]["position"][0].asInt();
        newSoundPosition.position.y = overridedPositions[i]["position"][1].asInt();

        soundPositions.push_back(newSoundPosition);
        setUseOriginalPositions();
    }

    Json::Value names = jsonData["clusterNames"];

    if(names != Json::nullValue){
        for(unsigned int i = 0; i < names.size(); i++){
           clusterNames.push_back(names[i].asString());
        }
    }

    if ( jsonData["DBScan_EPS"].isInt() ) {
        epsDbScan = jsonData["DBScan_EPS"].asInt();
    }

    if ( jsonData["DBScan_minPts"].isInt() ) {
        minPts = jsonData["DBScan_minPts"].asInt();
    }

    doClustering();

    Json::Value muted = jsonData["muted"];
    if (muted != Json::nullValue) {
        for (int i = 0; i < muted.size(); i++) {
            sounds[muted[i].asInt()]->mute = true;
        }
    }

}

string Sounds::selectFolder() {

    ofFileDialogResult dialogResult = ofSystemLoadDialog("Select Folder", true, ofFilePath::getUserHomeDir());

    string path;

    if(dialogResult.bSuccess) {
        path = dialogResult.getPath();
    }

    return path;
}

void Sounds::exportFiles() {

    string selectedFolderPath = selectFolder();

    if (selectedFolderPath.size() > 0) {

        selectedFolderPath = ofFilePath::addTrailingSlash(selectedFolderPath);

        for (std::set<int>::iterator it=clusterIds.begin(); it!=clusterIds.end(); ++it) {

            string clusterId = to_string(*it);
            string clusterFolderPath = selectedFolderPath + clusterId;

            ofDirectory dir(clusterFolderPath);
            if(!dir.exists()){
                dir.create(true);
            }
        }

        for(int i = 0; i < sounds.size(); i++) {

            if (sounds[i]->getCluster() >= 0 ) {

                string cluster = to_string( sounds[i]->getCluster() );
                string sourcePath = sounds[i]->getFileName();
                string destinationPath = selectedFolderPath + cluster;

                ofFile::copyFromTo(sourcePath, destinationPath, true, false);

            }
        }
        
        for (int i = 0; i < clusterNames.size(); i++) {
            
            if (clusterNames[i] != "") {
                
                string clusterFolderPath = selectedFolderPath + ofToString(i);
                ofDirectory dir(clusterFolderPath);
                
                if(dir.exists()){
                    string newFolderName = selectedFolderPath + ofToString(i) + " - " + clusterNames[i];
                    dir.renameTo(newFolderName);

                }
            }
        }
    }
}

void Sounds::oscListener(ofxOscMessage &m){
    TidalOscListener::getInstance()->onOscMsg(m);

    if(m.getAddress() == OSC_PLAY) {
        if ( m.getNumArgs() >= 2 ) {
            float volume = 1.0f;
            float x = m.getArgAsFloat(0) * initialWindowWidth;
            float y = m.getArgAsFloat(1) * initialWindowHeight;

            oscPosition = ofVec2f(x, y);

            if (m.getNumArgs() >= 3 ) {
                volume = m.getArgAsFloat(2);
            }

            oscPlay(oscPosition, volume);
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    } else if ( m.getAddress() == OSC_PLAY_X ) {
        if ( m.getNumArgs() >= 1 ) {
            float volume = 1.0f;
            float x = m.getArgAsFloat(0) * initialWindowWidth;
            oscPosition = ofVec2f(x, oscPosition.y);

            if (m.getNumArgs() >= 2) {
                volume = m.getArgAsFloat(1);
            }

            oscPlay(oscPosition, volume);
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    } else if ( m.getAddress() == OSC_PLAY_Y ) {
        if ( m.getNumArgs() >= 1 ) {
            float volume = 1.0f;
            float y = m.getArgAsFloat(0) * initialWindowHeight;
            oscPosition = ofVec2f(oscPosition.x, y);

            if (m.getNumArgs() >= 2) {
                volume = m.getArgAsFloat(1);
            }

            oscPlay(oscPosition, volume);
        }
    } else if ( m.getAddress() == OSC_PLAY_ID ) {
        if ( m.getNumArgs() >= 1 ) {
            float volume = 1.0f;

            if ( m.getNumArgs() >= 2 ) {
                volume = m.getArgAsFloat(1);
            }

            Sound * soundToPlay = getSoundById( m.getArgAsInt(0) );
            if ( soundToPlay != NULL ) {
                playSound( soundToPlay, volume );
            } else {
                ofLog() << "ID not found: " << m.getAddress();
            }
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    } else if ( m.getAddress() == OSC_PLAY_CLUSTER_BY_SOUND_ID) {
        if ( m.getNumArgs() >= 1 ) {
            int id = m.getArgAsInt(0);
            Sound * s = getSoundById( id );
            if ( s != NULL ) {
                if ( s->getCluster() > -1 ) {
                    vector<Sound *> cluster = getSoundsByCluster( s->getCluster() );
                    Sound * soundToPlay = nullptr;

                    if ( m.getNumArgs() >= 2 ) {
                        int soundIdx = m.getArgAsInt(1);
                        soundIdx = soundIdx % cluster.size();
                        soundToPlay = cluster[soundIdx];
                    } else { //if index is not provided choose random
                        do {
                            soundToPlay = cluster[ floor( ofRandom(0, cluster.size()) ) ];
                        } while( soundToPlay == s );
                    }

                    float volume = 1.0f;

                    if ( m.getNumArgs() >= 3 ) {
                        volume = m.getArgAsFloat(2);
                    }

                    playSound( soundToPlay, volume );
                } else {
                    ofLog() << "Sound is not in a cluster for OSC: " << m.getAddress();
                }
            }

        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    }

    else if(m.getAddress() == OSC_PLAY_CLUSTER){
        vector<Sound *>snds;
        string clusterName = "";
        int sndIdx = 0;
        float volume = 0.9f;

        if(m.getNumArgs() > 0){
            clusterName = m.getArgAsString(0);
            snds = getSoundsByCluster(clusterName);

            if ( snds.size() == 0 ) {
                ofLog() << "No sounds found in cluster for OSC: " << m.getAddress();
                return;
            }
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
            return;
        }
        //Sólo estoy mandando el cluster, elijo index randommente
        if(m.getNumArgs() == 1) {
            sndIdx = floor( ofRandom(0, snds.size()));
        }
        //Mando cluster e index
        if(m.getNumArgs() >= 2) {
            sndIdx = m.getArgAsInt(1);
            sndIdx = sndIdx % snds.size();
        }
        //Mando cluster index y volumen
        if(m.getNumArgs() == 3){
           volume = m.getArgAsFloat(2);
        }

        playSound(snds[sndIdx], volume);
    }

    /////////
    // Get num sounds
    /////////

    else if ( m.getAddress() == OSC_GET_NUM_SOUNDS ) {
        OscServer::sendMessage( OSC_GET_NUM_SOUNDS, (int)sounds.size() );
    }

    /////////
    // Get position by ID
    /////////

    else if ( m.getAddress() == OSC_GET_POSITION_ID ) {
        if ( m.getNumArgs() >= 1 ) {
            Sound * s = getSoundById( m.getArgAsInt(0) );
            ofxOscMessage msg;

            msg.setAddress( m.getAddress() );

            if ( s != NULL ) {
                // between 0 and 1
                msg.addFloatArg( s->getPosition().x / initialWindowWidth );
                msg.addFloatArg( s->getPosition().y / initialWindowHeight );
            } else {
                msg.addStringArg("ID not found");
            }
            OscServer::sendMessage( msg );
        }
    }

    /////////
    // Get neighbors
    /////////

    else if ( m.getAddress() == OSC_GET_NEIGHBORS_ID ) {
        if ( m.getNumArgs() >= 2 ) {
            Sound * s = getSoundById( m.getArgAsInt(0) );
            ofxOscMessage msg;
            msg.setAddress( OSC_GET_NEIGHBORS_ID );

            vector<Sound *> neighbors;

            if ( s != NULL ) {
                float threshold = pow(m.getArgAsFloat(1) * initialWindowWidth,2); //threshold between 0 and 1, square distance is cheaper than regular
                neighbors = getNeighbors(s, threshold );

                for ( unsigned int i = 0 ; i < neighbors.size() ; i++ ) {
                    msg.addIntArg( neighbors[i]->id );
                }
            } else {
                msg.addStringArg("ID not found");
            }

            OscServer::sendMessage( msg );
        } else {
            ofLog() << "Not enough arguments for OSC: " << m.getAddress();
            OscServer::sendMessage( m.getAddress(), "Not enough arguments" );
        }
    }
    else if ( m.getAddress() == OSC_GET_NEIGHBORS_XY ) {
        if ( m.getNumArgs() >= 3 ) {
            ofxOscMessage msg;
            msg.setAddress( OSC_GET_NEIGHBORS_XY );

            ofVec2f pos;
            pos.x = m.getArgAsFloat(0) * initialWindowWidth;
            pos.y = m.getArgAsFloat(1) * initialWindowHeight;

            vector<Sound *> neighbors;

            float threshold = pow(m.getArgAsFloat(2) * initialWindowWidth,2); //threshold between 0 and 1, square distance is cheaper than regular
            neighbors = getNeighbors( pos , threshold );

            for ( unsigned int i = 0 ; i < neighbors.size() ; i++ ) {
                msg.addIntArg( neighbors[i]->id );
            }

            OscServer::sendMessage( msg );
        } else {
            ofLog() << "Not enough arguments for OSC: " << m.getAddress();
            OscServer::sendMessage( m.getAddress(), "Not enough arguments" );
        }
    }

    /////////
    // Enable Get played sound ID
    /////////

    else if ( m.getAddress() == OSC_GET_PLAYED_SOUND_ID ) {
        if ( m.getNumArgs() >= 1 ) {
            oscGetPlayedSoundIDEnabled = (bool)( m.getArgAsInt(0) );
        }
    }

    /////////
    // Enable Get cluster ID by sound ID
    /////////

    else if ( m.getAddress() == OSC_GET_CLUSTER_ID ) {
        if ( m.getNumArgs() >= 1 ) {
            int soundID = m.getArgAsInt(0);

            Sound * s = getSoundById(soundID);
            if ( s != NULL ) {
                OscServer::sendMessage( m.getAddress(), s->getCluster() );
            } else {
                OscServer::sendMessage( m.getAddress(), "Sound ID not found" );
            }
        } else {
            OscServer::sendMessage( m.getAddress(), "Not enough arguments" );
        }
    }

    /////////
    // Enable Get cluster name by sound ID
    /////////

    else if ( m.getAddress() == OSC_GET_CLUSTER_NAME ) {
        if ( m.getNumArgs() >= 1 ) {
            int soundID = m.getArgAsInt(0);

            Sound * s = getSoundById(soundID);
            if ( s != NULL ) {
                int clusterID = s->getCluster();
                if ( clusterID > 0 && clusterID < clusterNames.size() ) {
                    OscServer::sendMessage( m.getAddress(), clusterNames[clusterID] );
                } else {
                    OscServer::sendMessage( m.getAddress(), "Cluster ID not found" );
                }
            } else {
                OscServer::sendMessage( m.getAddress(), "Sound ID not found" );
            }
        } else {
            OscServer::sendMessage( m.getAddress(), "Not enough arguments" );
        }
    }

    /////////
    // Enable Get cluster name by cluster ID
    /////////

    else if ( m.getAddress() == OSC_GET_CLUSTER_NAME_BY_CLUSTER_INDEX ) {
        if ( m.getNumArgs() >= 1 ) {
            int clusterID = m.getArgAsInt(0);
            if ( clusterID > 0 && clusterID < clusterNames.size() ) {
                OscServer::sendMessage( m.getAddress(), clusterNames[clusterID] );
            } else {
                OscServer::sendMessage( m.getAddress(), "Cluster ID not found" );
            }
        } else {
            OscServer::sendMessage( m.getAddress(), "Not enough arguments" );
        }
    }


}

void Sounds::oscPlay(ofVec2f position, float volume) {

    Sound * nearestSound = getNearestSound(position);

    if ( nearestSound != NULL ) {
        oscOpacity = 255;
        playSound( nearestSound, volume );
    }
}

void Sounds::oscDraw() {
    if ( oscOpacity < 1 ) return;

    ofNoFill();
    ofSetColor(255,255,255, oscOpacity);
    ofDrawLine((oscPosition.x), (oscPosition.y-4.5), (oscPosition.x), (oscPosition.y+4.5));
    ofDrawLine((oscPosition.x-4.5), (oscPosition.y), (oscPosition.x+4.5), (oscPosition.y));
}

void Sounds::keyPressed(ofKeyEventArgs & e){

    if(e.key == CLUSTERNAME_SHOW_SHORTCUT && !showClusterNames) {
        showClusterNames = true;
        for (unsigned int i = 0; i < sounds.size(); i++) {
            sounds[i]->clusterIsHovered = false;
        }
    }
}

void Sounds::keyReleased(int key){
    if(key == CLUSTERNAME_SHOW_SHORTCUT) {
        showClusterNames = false;
        for (unsigned int i = 0; i < sounds.size(); i++) {
            sounds[i]->clusterIsHovered = true;
        }
    }
}

void Sounds::nameCluster(unsigned int clusterID){
        clusterNames[clusterID] = ofToString(clusterNameBeingEdited);
        clusterNameBeingEdited[0] = '\0';
}

bool Sounds::areAnyClusterNames(){
    for(unsigned int i = 0; i < clusterNames.size(); i++){
        if(clusterNames[i] != "") return true;

    }
    return false;
}

void Sounds::deleteClusterNames(){
    for(unsigned int i = 0; i < clusterNames.size(); i++){
       clusterNames[i]  = "";
    }
}

bool Sounds::isMouseHoveringGUI() {

    return isClusteringOptionsScreenHovered;

}
