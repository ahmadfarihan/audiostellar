#ifndef _UTILS
#define _UTILS
#include "ofMain.h"

class Utils {
public:
    static const int DISTANCE_TRESHOLD = 5;

    //TODO esto debería ir en MidiServer
    struct midiMsg{
      string status;
      int pitch;
      int cc;
      int ccVal;
      int beats = -1;
      double bpm = -1;
    };



    static float distance(ofVec2f p1, ofVec2f p2) {
        ofVec2f diff = p1 - p2;
        float dist = diff.length();
        return dist;
    }
    static float getMaxVal(float val, float winner) {
        if(val > winner) {
            return val;
        } else {
            return winner;
        }
    }
    static float getMinVal(float val, float winner) {
        if(val < winner) {
            return val;
        } else {
            return winner;
        }
    }

    static string resolvePath(string root, string path){

        if (!ofFilePath::isAbsolute(path)) {
            root = ofFilePath::getEnclosingDirectory(root);
            path = root + path;
        }

        if(path[path.size() - 1] != '/'){
            path = path + "/";
        }

        return path;

    }
};
#endif
