#include "Tooltip.h"

bool Tooltip::enabled = true;
bool Tooltip::itemHovered = false;
string Tooltip::tooltipTitle = "";
string Tooltip::tooltipContent = "";

void Tooltip::setTooltip(struct tooltip tooltip) {
    
    itemHovered = true;
    tooltipTitle = tooltip.title;
    tooltipContent = tooltip.content;
    
}

void Tooltip::drawGui() {
    
    if (enabled) {
        
        if (itemHovered) {
            
            auto mainSettings = ofxImGui::Settings();
            const float DISTANCE = 10.0f;
            static int corner = 2;
            ImVec2 window_pos = ImVec2((corner & 1) ? ImGui::GetIO().DisplaySize.x - DISTANCE : DISTANCE, (corner & 2) ? ImGui::GetIO().DisplaySize.y - DISTANCE : DISTANCE);
            ImVec2 window_pos_pivot = ImVec2((corner & 1) ? 1.0f : 0.0f, (corner & 2) ? 1.0f : 0.0f);
            if (corner != -1)
                ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
            
            //        ImGui::PushStyleColor(ImGuiCol_TitleBg, ImVec4(0.1f, 0.1f, 0.1f, 1.00f));
            
            if (ImGui::Begin(tooltipTitle.c_str(), NULL, (corner != -1 ? ImGuiWindowFlags_NoMove : 0) |ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav))
            {
                float wrap_width = ofGetWindowWidth() - 20.0f;
                ImGui::PushTextWrapPos(wrap_width);
                ImGui::Text(tooltipContent.c_str(), wrap_width);
                ImGui::PopTextWrapPos();
            }
            
            //        ImGui::PopStyleColor(1);
            
            ImGui::End();
        }
        itemHovered = false;
    }
}
