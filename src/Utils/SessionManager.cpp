#include "SessionManager.h"



SessionManager::SessionManager(Modes * modes, MidiServer * midiServer){

#ifdef TARGET_LINUX
    SETTINGS_FILENAME = ofFilePath::getUserHomeDir() + "/.config/" + SETTINGS_FILENAME;
#else
    SETTINGS_FILENAME = ofToDataPath(SETTINGS_FILENAME);
#endif
    sounds = Sounds::getInstance();
    this->modes = modes;
    this->midiServer = midiServer;
    audioEngine = AudioEngine::getInstance();
    voices = Voices::getInstance();
    
    loadSettings();

}

void SessionManager::loadInitSession()
{
    //Load last recent project
    if(areAnyRecentProjects(recentProjects)){
        datasetLoadOptions opts;
        opts.method = "RECENT";
        opts.path = recentProjects[recentProjects.size() - 1];
        loadSession(opts);
    }

    if ( !getSessionLoaded() ) {
        loadDefaultSession();
    }
}

void SessionManager::loadSettings(){
    if(!settingsFileExist()){
      createSettingsFile();
    }

    //LOAD FILE
    ofxJSONElement file;
    bool loaded = file.open(SETTINGS_FILENAME);

    //VALIDATION
    if(!loaded){
    ofLogNotice("ERROR", "error loading settings json file");
    }

    if(file["recentProjects"]       == Json::nullValue ||
       file["lastMidiDevice"]       == Json::nullValue ||
       file["replaySound"]          == Json::nullValue ||
       file["useOriginalPositions"] == Json::nullValue ||
       file["showSoundFilenames" ]  == Json::nullValue ||
       file["audioSettings" ]       == Json::nullValue ||
       file["numVoices" ]           == Json::nullValue) {

        ofLogNotice("ERROR",
                    "corrupted settings file. Deleting and creating new...");

        ofFile::removeFile(SETTINGS_FILENAME);
        createSettingsFile();

    } else {
        //seteamos la global
        settingsFile = file;
    }

    //SET SETTINGS ACCORDING TO FILE

    //recentProjects
    recentProjects = loadRecentProjects();

    //set lastMidiDevice
    if(settingsFile["lastMidiDevice"] != ""){
      lastSavedMidiDevice = settingsFile["lastMidiDevice"].asString();
      midiServer->setCurrentMidiDevice(lastSavedMidiDevice);
    }

    //ReplaySound
    bool rSound = settingsFile["replaySound"].asBool();
    sounds->setReplaySound(rSound);

    //Use original Positions
    bool useOrigP = settingsFile["useOriginalPositions"].asBool();
    sounds->setUseOrigPos(useOrigP);

    //Show sound filenames
    bool showFilenames = settingsFile["showSoundFilenames"].asBool();
    sounds->setShowSoundFilenames(showFilenames);

    //OSC
    if ( settingsFile["OSC_receivePort"].asInt() > 0 ) {
        OscServer::setReceivePort( settingsFile["OSC_receivePort"].asInt() );
    }
    if ( settingsFile["OSC_sendPort"].asInt() > 0 ) {
        OscServer::setSendPort( settingsFile["OSC_sendPort"].asInt() );
    }
    if ( settingsFile["OSC_sendHost"].asString() != "" ) {
        OscServer::setSendHost( settingsFile["OSC_sendHost"].asString() );
    }
    if ( settingsFile["OSC_enable"].asBool() ) {
        OscServer::getInstance()->start();
    }
    
    //Audio settings
    audioEngine->load(settingsFile);
    
    if ( settingsFile["numVoices"].asInt() > 0 ) {
        voices->numVoices = settingsFile["numVoices"].asInt();
    }

}

void SessionManager::loadSession(datasetLoadOptions opts){
    string datasetPath;

    isDefaultSession = opts.path == ofToDataPath(DEFAULT_SESSION, true);

    //OPENING
    if(opts.method == "GUI"){
        datasetPath = userSelectJson();
        if(datasetPath == "CANCELLED"){
            return;
        }
    }else if(opts.method == "TERMINAL" ||
             opts.method == "RECENT"){
        datasetPath = opts.path;
    }
    //VALIDATION
    datasetResult loadingDatasetResult = validateDataset(datasetPath);

    //SHOW ERROR IN CASE THERE IS ONE
    if(!loadingDatasetResult.success){
        Gui::getInstance()->showMessage(
                    loadingDatasetResult.status.c_str(),
                    STRING_ERROR.c_str()
                    );
    }else{
        //SUCCESSFULL LOADING
       sessionLoaded = true;
       ofxJSONElement file = loadingDatasetResult.data;
       string audioFilesPath = file["audioFilesPath"].asString();

       if ( audioFilesPath == DEFAULT_SESSION_PLACEHOLDER ) {
           audioFilesPath = ofToDataPath( DEFAULT_SESSION_DIRECTORY, true );
       }

       //Safety measure with paths
       file["audioFilesPathAbsolute"] = Utils::resolvePath
                                    (datasetPath,
                                     audioFilesPath);

       jsonFile = file;
       jsonFilename = datasetPath;

       if (!isDefaultSession) {
           saveToRecentProject(datasetPath);
       }

       //LOAD SOUNDS
       sounds->loadSounds(file);

       //RESET AND LOAD MODES DATA (MIDI MAPPINGS AND SUCH)
       Json::Value jsonModes = file["modes"];

       for ( unsigned int i = 0 ; i < modes->modes.size() ; i++ ) {
            modes->modes[i]->reset();
       }

       if ( jsonModes != Json::nullValue ) {
           for ( unsigned int i = 0 ; i < modes->modes.size() ; i++ ) {
               modes->modes[i]->load( jsonModes[ modes->modeNames[i] ] );
           }
       }

       //RESET MIDI AND LOAD MAPPING
       midiServer->reset();
       midiServer->load(file["midiCC"]);

       //LOAD SOUND OPTIONS (REPLAY SOUND AND SUCH)
       sounds->load(file["sounds"]);

       //SET WIN TITLE
       setWindowTitleWithSessionName();
    }

}

string SessionManager::userSelectJson(){
        string filename;
        ofFileDialogResult selection = ofSystemLoadDialog("Select JSON file");

        if(selection.bSuccess){
            filename = selection.getPath();
        }else{
            filename = "CANCELLED";
        }
        return filename;

}

SessionManager::datasetResult SessionManager::validateDataset(string datasetPath){
   datasetResult res;
   string extension = ofFilePath::getFileExt(datasetPath);

   //Check extension
   if(extension != "json"){
       res.success = false;
       res.status = "This is not a JSON file";
       return res;
   }

   //Check if valid json
   bool successLoading = res.data.open(datasetPath);
   if(!successLoading){
      res.success = false;
      res.status = "Error loading JSON: " + datasetPath;
      return res;
   }

   //Check if minimum fields exist
   Json::Value soundFilesPath = res.data["audioFilesPath"];
   Json::Value map = res.data["tsv"];

   if(soundFilesPath == Json::nullValue || map == Json::nullValue){
       res.success = false;
       res.status = "Not a valid sound map";
       return res;
   }

   res.success = true;
   res.status = "OK";
   return res;

}


void SessionManager::loadDefaultSession(){
    if ( ofFile::doesFileExist( ofToDataPath( DEFAULT_SESSION, true ) ) ) {
        datasetLoadOptions opts;
        opts.method = "RECENT";
        opts.path = ofToDataPath(DEFAULT_SESSION, true);
        loadSession(opts);
        Gui::getInstance()->showWelcomeScreen();
    } else {
        Gui::getInstance()->showMessage(
                    "Default session not found at " +
                    ofToDataPath( DEFAULT_SESSION, true ) +
                    ".\n\n"
                    "If you've compiled from source, make sure its inside bin/data/",
                    "Warning" );
    }

}


void SessionManager::saveSettingsFile(){
  Json::Value root = Json::Value(Json::objectValue);

   //Recent Projects
   Json::Value recent = Json::Value(Json::arrayValue);
   for(unsigned int i = 0 ; i < recentProjects.size(); i++){
       recent[i] = recentProjects[i];
   }

   root["recentProjects"] = recent;

   //Last midi device
   string currentMidiDevice = midiServer->getCurrentMidiDevice();
   if (currentMidiDevice != "Not set"){
    root["lastMidiDevice"] = currentMidiDevice;
  }else if(lastSavedMidiDevice != ""){
    root["lastMidiDevice"] = lastSavedMidiDevice;
  }else{
    root["lastMidiDevice"] = "";
  }

   //ReplaySound
   int replaySound = sounds->getReplaySound() ? 1 : 0;
   root["replaySound"] = replaySound;

   //Use original positions
   int useOrigPos = sounds->getUseOriginalPositions() ? 1 : 0;
   root["useOriginalPositions"] = useOrigPos;

   //Show sound filenames
   int showSoundFilenames = sounds->getShowSoundFilenames() ? 1 : 0;
   root["showSoundFilenames"] = showSoundFilenames;

   //OSC
   root["OSC_enable"] = OscServer::enable;
   root["OSC_receivePort"] = OscServer::getReceivePort();
   root["OSC_sendPort"] = OscServer::getSendPort();
   root["OSC_sendHost"] = OscServer::getSendHost();
    
    //Audio settings
    root["audioSettings"] = audioEngine->save();
    
    root["numVoices"] = voices->numVoices;
    
   settingsFile = root;
   settingsFile.save(SETTINGS_FILENAME, true);
}
bool SessionManager::settingsFileExist(){
  return ofFile::doesFileExist(SETTINGS_FILENAME);
}

void SessionManager::createSettingsFile(){
  Json::Value value = Json::Value (Json::objectValue);

  value["recentProjects"] = Json::Value(Json::arrayValue);
  value["lastMidiDevice"] = "";
  value["replaySound"] = Json::Value(Json::intValue);
  value["useOriginalPositions"] = Json::Value(Json::intValue);
  value["showSoundFilenames"] = Json::Value(Json::intValue);

  value["OSC_enable"] = Json::Value(Json::booleanValue);
  value["OSC_receivePort"] = Json::Value(Json::intValue);
  value["OSC_sendPort"] = Json::Value(Json::intValue);
  value["OSC_sendHost"] = Json::Value(Json::stringValue);
    
  value["audioSettings"] = Json::Value(Json::objectValue);
  value["numVoices"] = Json::Value(Json::intValue);

  //seteamos la global
  settingsFile = value;
  settingsFile.save(SETTINGS_FILENAME, true);
}

vector<string> SessionManager::loadRecentProjects(){
    vector<string> r;
    Json::Value recent = Json::Value(Json::arrayValue);

    recent = settingsFile["recentProjects"];
    for(unsigned int i = 0; i < recent.size(); i ++){
      if(recent[i] != Json::nullValue){
        r.push_back(recent[i].asString());
      }
    }

    return r;

}

void SessionManager::saveToRecentProject(string path){
    //Si no hay recientes
    if(recentProjects.size() == 0){
        recentProjects.push_back(path);
    } else {
        for(unsigned int i = 0 ; i < recentProjects.size(); i++){
            if(path == recentProjects[i]){
                recentProjects.erase( recentProjects.begin()+i );
                break;
            }
        }

        recentProjects.push_back(path);
    }
}


void SessionManager::saveSession() {
    ofLog() << "Saving...";

    /****
        Sounds
    ****/

    Json::Value jsonSounds = jsonFile["sounds"];

    if ( jsonSounds == Json::nullValue ) {
        jsonFile["sounds"] = Json::Value( Json::objectValue );
    }

    jsonFile["sounds"] = sounds->save();


    /****
        MODES
    ****/

    Json::Value jsonModes = jsonFile["modes"];

    if ( jsonModes == Json::nullValue ) {
        jsonFile["modes"] = Json::Value( Json::objectValue );
    }

    for ( int i = 0 ; i < modes->modeNames.size() ; i++ ) {
        jsonFile["modes"][ modes->modeNames[i] ] = modes->modes[i]->save();
    }

    /****
        MIDI CC
    ****/

    Json::Value jsonMidiCC = jsonFile["midiCC"];

    if ( jsonMidiCC == Json::nullValue ){
      jsonFile["midiCC"] = Json::Value( Json::arrayValue);
    }

    jsonFile["midiCC"] = midiServer->save();

    // I don't want to save the absolute path, I remove it and then add again
    Json::Value audioFilesPathAbsolute = jsonFile.removeMember("audioFilesPathAbsolute");

    jsonFile.save(jsonFilename, true);

    jsonFile["audioFilesPathAbsolute"] = audioFilesPathAbsolute;

    ofLog() << "Done saving";
    Gui::getInstance()->showMessage("Session saved successfully", "Information", true);
}

void SessionManager::saveAsNewSession(){
  ofFileDialogResult result = ofSystemSaveDialog(
              ofFilePath::getEnclosingDirectory(jsonFilename) + "/new_session.json", "Save");

  if(result.bSuccess){
    string path = result.getPath();
    
    if(!ofIsStringInString(path, ".json")){
        path = path + ".json";
    }

    jsonFilename = path;

    if ( isDefaultSession ) {
        jsonFile["audioFilesPath"] = DEFAULT_SESSION_PLACEHOLDER;
    }

    isDefaultSession = false;
    saveSession();
    setWindowTitleWithSessionName();
    saveToRecentProject(jsonFilename);
  }
}


vector <string> SessionManager::getRecentProjects(){
        vector<string> invertedRecent;
        for(int i = recentProjects.size() - 1; i >= 0; i --){
                invertedRecent.push_back(recentProjects[i]);
        }
        return invertedRecent;
}

bool SessionManager::getSessionLoaded(){
    return sessionLoaded;
}

bool SessionManager::areAnyRecentProjects(vector<string> recentProjects){
   return recentProjects.size() > 0 &&
           recentProjects[recentProjects.size() - 1] != "";
}

void SessionManager::exit(){
   saveSettingsFile();
   ofLogNotice("Written settings files");

}

void SessionManager::setWindowTitleWithSessionName(){
    ofSetWindowTitle("AudioStellar // " +
                     ofFilePath::getFileName( jsonFilename ) +
                     " (" + ofToString(sounds->getSounds().size()) + " sounds)" );
}

void SessionManager::keyPressed(ofKeyEventArgs & e) {
    
}
