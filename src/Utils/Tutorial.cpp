#include "Tutorial.h"

Tutorial::Tutorial()
{
    TutorialPage page1;
    page1.title = "The sound map";
    page1.content = "What you are watching right now is a sound map made from more than 2000 drumkit sounds. \n\n"
                    "Each point is a short audio sample. \n\n"
                    "Press left mouse button while moving the mouse to hear whatever sound is behind the pointer. \n\n"
                    "Scroll up and down for zooming in and out.\n\n"
                    "Press middle mouse button or <space> while moving the mouse to pan the camera. \n\n"
                    "Hold 'c' on your keyboard to show cluster names. You can set names by right clicking a cluster. \n\n"
                    "You can also right click on individual sounds and clusters to get options. \n\n"
                    "Explore how similar sounds share a color and are clustered together in latent space. \n\n";

    TutorialPage page2;
    page2.title = "Three modes to rule them all";
    page2.content = "Right now Explorer Mode is activated. On the top center of the screen you'll find three buttons that let you select the active mode. \n\n"
                    "Each of these modes offer a different way to interact with the map. \n\n"
                    "Click 'Next' to learn more.\n\n";

    TutorialPage page3;
    page3.title = "The particle mode";
    page3.content = "Click on the second mode to activate Particle Mode. \n\n"
                    "Clicking anywhere multiple times on the map will cast particles. They will play everything they touch.\n\n"
                    "These are autonoums agents that are governed by a set of parameters you can tweak using the Tools menu. \n\n"
                    "Select 'Explosion' in the Tools menu and click anywhere in the map to try a different particle model. \n\n"
                    "All this actions can also be done using an external MIDI interface.\n\n";

    TutorialPage page4;
    page4.title = "The sequence mode";
    page4.content = "Click on the third mode to activate Sequence Mode. \n\n"
                    "Clicking on the points on the map will create a constellation and selected samples will be play in a sequence.\n\n"
                    "The rhythm will be created according to the distance between points.\n\n"
                    "Using the Tools menu you can create multiple sequences and adjust their volume and other parameters.\n\n"
                    "MIDI clock can be synced with other applications and hardware \n\n";

    TutorialPage page5;
    page5.title = "Have fun!";
    page5.content = "There is much more of AudioStellar than this: MIDI, OSC and JACK support allows to connect with other applications and interfaces.\n\n"
                    "Every element of the user interface has a helpful tooltip that appears automatically when placing the mouse pointer over. This will help you learn more about AudioStellar.\n\n"
                    "The real adventure begins when creating your own sound map, try it using the File menu and clicking New.\n\n"
                    "Find more info at http://www.audiostellar.xyz\n\n";

    pages.push_back(page1);
    pages.push_back(page2);
    pages.push_back(page3);
    pages.push_back(page4);
    pages.push_back(page5);
}

Tutorial::TutorialPage * Tutorial::getNextPage()
{
    currentPage++;

    if ( currentPage < pages.size() ) {
        return &(pages[currentPage]);
    }

    return nullptr;
}

bool Tutorial::isLastPage()
{
    return currentPage == pages.size() -1;
}

Tutorial::TutorialPage * Tutorial::start()
{
    currentPage = 0;
    return &(pages[currentPage]);
}
