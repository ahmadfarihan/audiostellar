#pragma once
#include "ofMain.h"

class Tutorial
{
public:
    struct TutorialPage {
        string title;
        string content;
    };

    vector<TutorialPage> pages;

    int currentPage = -1;

    Tutorial();

    TutorialPage * getNextPage();
    bool isLastPage();
    TutorialPage * start();


};
