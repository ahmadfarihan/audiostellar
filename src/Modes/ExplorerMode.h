#pragma once

#include "ofMain.h"
#include "Mode.h"
#include "Sounds.h"
#include "MidiServer.h"

class ExplorerMode: virtual public Mode {

private:
    Sounds *sounds;

    //nota midi -> idSonido
    std::unordered_map<int, int> midiMappings;
    Sound * midiTriggeredSound = nullptr;

public:
    ExplorerMode(Sounds *_sounds);

    void update();
    void reset();
    void mousePressed(ofVec2f p, int button);
    void midiMessage(Utils::midiMsg m);

    Json::Value save();
    void load( Json::Value jsonData );
    
    bool showContextMenu = false;
    string hoveredSoundPath;
    
    void drawGui();
    
    bool isContextMenuHovered = false;
    
    bool isMouseHoveringGui();
    
};
