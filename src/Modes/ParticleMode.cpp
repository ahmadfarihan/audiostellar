#include "ParticleMode.h"
/*
##################################################################
                       PARTICLE MODE
##################################################################
*/
ParticleMode::ParticleMode(Sounds *_sounds, MidiServer *_midiServer) {
    sounds = _sounds;
    midiServer = _midiServer;

    modeName = "Particle Mode";
    //    iconPath = "assets/icons/particle_mode.png";
    iconPath = "assets/icons/particle_mode_tinteable.png";
    
    drawer = new ParticleRegionsManager();
    
    //modelNames.push_back("Simple");
    modelNames.push_back("Swarm");
    modelNames.push_back("Explosion");
    
    //modeTooltips.push_back(Tooltip::SIMPLE_MODE);
    modeTooltips.push_back(Tooltip::PARTICLE_SWARM);
    modeTooltips.push_back(Tooltip::PARTICLE_EXPLOSION);
    
    // para cuando este listo
    //    modelNames.push_back("Attractor");
    
    atModel = 0;
    
    //Global particle params
    age = 9.0f;
    randomizeEmitter = false;
    
    ofAddListener(OscServer::oscEvent, this, &ParticleMode::oscDispatcher);

    
    
}

void ParticleMode::reset(){

  if(!drawer->areas.empty()){
      drawer->areas.clear();
  }

  if(!particles.empty()){
      particles.clear();
  }
}


vector<Particle*> ParticleMode::add(ofVec2f p){
    
    vector<Particle*> ps;
    

    if(modelNames[atModel] == "Simple") {
        ps.push_back(new SimpleParticle());
        ps[0]->setPosition(p);
        particles.push_back(ps[0]);
        
    } else if(modelNames[atModel] == "Swarm") {
        ps.push_back(new SpreadedParticle());
        ps[0]->setPosition(p);
        particles.push_back(ps[0]);
        
    } else if(modelNames[atModel] == "Explosion") {
        for(int i = 0; i< RadialParticle::density; i++) {
            ps.push_back(new RadialParticle());
            ps[i]->setPosition(p);
            particles.push_back(ps[i]);
        }
    } /*else if(modelNames[atModel] == "Attractor"){
        ps.push_back(new AttractedParticle());
        ps[0]->setPosition(p);
        particles.push_back(ps[0]);

    }*/

    return ps;
}
/*
void ParticleMode::addAttractor(ofVec2f p){
    if(modelNames[atModel] == "Attractor"){
        AttractorParticle *attractor  = new AttractorParticle();
        attractor->setPosition(p);
        attractors.push_back(attractor);
    }
}
*/
void ParticleMode::update() {
    
    for (int i = 0; i < particles.size(); i++) {
        Sound * particleNearestSound = sounds->getNearestSound(particles[i]->getPosition());
        if ( particleNearestSound != NULL &&
             particleNearestSound != particles[i]->lastPlayedSound ) {

            particles[i]->lastPlayedSound = particleNearestSound;
            sounds->playSound(particleNearestSound, volume);
        }

        particles[i]->age = age;
        particles[i]->update();
        
        if(modelNames[atModel] == "Attractor"){
            /*

            
            for(int j = 0; j < attractors.size(); j++){
                
                ofVec2f f = attractors[j]->attract(particles[i]);
                
                particles[i]->applyForce(f);
                
                if(particles[i] != NULL &&
                   (particles[i]->crashes(attractors[j]->getPosition()) || particles[i]->isDead())){
                    delete particles[i];
                    particles.erase(particles.begin() + i);
                    break;
                }
                
            }
            */
        }else{
            
            if (particles[i]->isDead()) {
                delete particles[i];
                particles.erase(particles.begin() + i);
            }
        }
        
    }
    drawer->update();

    if(!MidiServer::midiLearn){
        if(drawer->getLastArea() != NULL){
            if(!drawer->getLastArea()->assigned){
                drawer->destroy();
            }
        }
    }
}

void ParticleMode::beforeDraw(){
    
    if(modelNames[atModel] == "Attractor"){
        /*
        for(int i = 0; i < attractors.size(); i++){
            attractors[i]->customDraw();
        }
        */
    }
    
    drawer->draw();
}

void ParticleMode::draw() {
    for (int i = 0; i < particles.size(); i++) {
        particles[i]->draw();
    }
    
}

void ParticleMode::drawGui(){
    auto mainSettings = ofxImGui::Settings();

    if(ofxImGui::BeginTree(modeName, mainSettings)) {
        //Model selector
        for(int i = 0; i < modelNames.size(); i++){
            if(ImGui::RadioButton(modelNames[i].c_str(), &atModel , i)){
                atModel = i;
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(modeTooltips[i]);

            //no le tires same line al último
            if(i != modelNames.size() - 1){
                ImGui::SameLine();
            }
        }

        ImGui::NewLine();

        if(midiServer->SliderFloat("Volume", &volume, 0.0f, 1.0f)) {
            OscServer::sendMessage(OSC_PARTICLE_VOL, volume);
        }
        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::PARTICLE_VOLUME);

        if(midiServer->SliderFloat("Lifespan", &age, 0.0f, 10.0f)) {
            OscServer::sendMessage(OSC_PARTICLE_LIFESPAN, age);
        }
        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::PARTICLE_LIFESPAN);

        ImGui::Checkbox("Randomize Emitter", &randomizeEmitter);
        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::PARTICLE_RANDOMIZE_EMITTER);

        ImGui::NewLine();

        if(modelNames[atModel] == "Simple"){
            SimpleParticle::drawGui(midiServer);
        }else if(modelNames[atModel] == "Swarm"){
            SpreadedParticle::drawGui(midiServer);
        } else if(modelNames[atModel] == "Explosion"){
            RadialParticle::drawGui(midiServer);
        }else if(modelNames[atModel] == "Attractor"){
            //AttractorParticle::drawGui();
            //AttractedParticle::drawGui();
        }

       if(drawer->getLastAssignedArea() != NULL){
           if ( ImGui::Button("Destroy last area") ) {
               //deleteLastParticleRegion();
               drawer->destroyLastAssignedArea();
           }
       }

       ImGui::NewLine();
       ofxImGui::EndTree(mainSettings);
    }
    
}

void ParticleMode::mousePressed(ofVec2f p, int button) {
    
    
    if(MidiServer::midiLearn) {
        if(drawer->getLastArea() != NULL){
            if(!drawer->getLastArea()->assigned){
                drawer->destroy();
            }
        }
    } else {
        if(button == 0) {//boton izquierdo
            add(p);
        }else if(button == 2){//boton derecho
            if(modelNames[atModel] == "Attractor"){
                //ofLog() << "new Attractor";
                //addAttractor(p);
            }
        }
        
    }
}
void ParticleMode::mouseDragged(ofVec2f p, int button) {
    
    if(MidiServer::midiLearn) {
        drawer->mouseDragged(p);
    } else {
        if(button == 0) {
            add(p);
        }
    }
}
void ParticleMode::mouseReleased(ofVec2f p) {
    if(MidiServer::midiLearn) {
        drawer->mouseReleased();
    }
}
void ParticleMode::keyPressed(int key) {

}

void ParticleMode::midiMessage(Utils::midiMsg m) {
    
    if(m.status == "Note On") {
        drawer->onNoteOn(m.pitch);
        if(drawer->status == "idle"){
            for(int i = 0;i< drawer->areas.size();i++){
                if(drawer->areas[i]->note == m.pitch){
                    vector<Particle*> ps;
                    
                    if(randomizeEmitter){
                        ps = add(drawer->areas[i]->getRandomPointInside());
                    }else{
                        ps = add(drawer->areas[i]->centerPoint);
                    }
                    
                    for(int j = 0; j< ps.size(); j++){
                        ps[j]->setArea(drawer->areas[i]);
                    }
                    MidiServer::midiLearn = false;
                    //btnMidiLearn->setChecked(false);
                }
            }
        }
    }else if(m.status == "Control Change"){
        if(MidiServer::midiLearn){
            
            //btnMidiLearn->setChecked(false);
        }
    }
}

Json::Value ParticleMode::save(){
    Json::Value root = Json::Value(Json::objectValue);
    Json::Value midiMappings = Json::Value(Json::arrayValue);
    
    for(unsigned int i = 0; i< drawer->areas.size(); i++){
        ParticleRegion* area = drawer->areas[i];
        Json::Value midiMapping = Json::Value(Json::objectValue);
        midiMapping["Note"] = area->note;

        ofVec3f center = sounds->camToSoundCoordinates( area->centerPoint );
        midiMapping["CenterX"] = center.x;
        midiMapping["CenterY"] = center.y;

        midiMapping["Radius"] = abs(area->radius); //this should be normalized as well somehow
        midiMappings[i] = midiMapping;
    }
    root["midiMappings"] =  midiMappings;
    return root;
    
}

void ParticleMode::load(Json::Value jsonData){
  Json::Value midiMappings = jsonData["midiMappings"];

  if(midiMappings != Json::nullValue) {
    for(unsigned int i = 0 ; i < midiMappings.size(); i++){
      Json::Value midiMapping = midiMappings[i];
      ParticleRegion* area = new ParticleRegion();

      area->assigned = true;
      area->note = midiMapping["Note"].asInt();

      ofVec3f center = sounds->soundToCamCoordinates( ofVec3f( midiMapping["CenterX"].asFloat(), midiMapping["CenterY"].asFloat(), 0 ) );
      area->centerPoint.x = center.x;
      area->centerPoint.y = center.y;

      area->radius = midiMapping["Radius"].asFloat(); //this should be normalized as well somehow

      drawer->areas.push_back(area);
    }
  }
}

void ParticleMode::deleteLastParticleRegion()
{
    drawer->destroyLastArea();
}


void ParticleMode::oscDispatcher(ofxOscMessage &m){
    /////////
    // Play XY
    /////////
    
    if(m.getAddress() == OSC_EMIT) {
        if ( m.getNumArgs() >= 2 ) {
            float x = m.getArgAsFloat(0) * ofGetWidth();
            float y = m.getArgAsFloat(1) * ofGetHeight();
        
            add(ofVec2f(x, y));
        }
    }
    
/*    
    } else if ( m.getAddress() == OSC_EMIT_X ) {
        float x = m.getArgAsFloat(0) * ofGetWidth();
        
        add(ofVec2f(x, y));
    
    } else if ( m.getAddress() == OSC_EMIT_Y ) {
        float y = m.getArgAsFloat(0) * ofGetHeight();
        
        add(ofVec2f(x, y);
        
    }
 */
    


    if(m.getAddress() == OSC_PARTICLE_VOL) {
        if ( m.getNumArgs() >= 1 ) {
            volume = m.getArgAsFloat(0);
        }
    }
    
    if(m.getAddress() == OSC_PARTICLE_LIFESPAN) {
        if ( m.getNumArgs() >= 1 ) {
            age = m.getArgAsFloat(0);
        }
    }
    
    if(m.getAddress() == SpreadedParticle::OSC_PARTICLE_SWARM_VEL) {
        if ( m.getNumArgs() >= 2 ) {
            SpreadedParticle::acceleration.x = m.getArgAsFloat(0);
            SpreadedParticle::acceleration.y = m.getArgAsFloat(1);

        }
    }
    
    if(m.getAddress() == SpreadedParticle::OSC_PARTICLE_SWARM_VEL_X) {
        if ( m.getNumArgs() >= 1 ) {
            SpreadedParticle::acceleration.x = m.getArgAsFloat(0);
        }
    }
    
    if(m.getAddress() == SpreadedParticle::OSC_PARTICLE_SWARM_VEL_Y) {
        if ( m.getNumArgs() >= 1 ) {
            SpreadedParticle::acceleration.y = m.getArgAsFloat(0);
        }
    }
    
    if(m.getAddress() == SpreadedParticle::OSC_PARTICLE_SWARM_SPREAD) {
        if ( m.getNumArgs() >= 1 ) {
            SpreadedParticle::spreadAmt = m.getArgAsFloat(0);
        }
    }
    
    if(m.getAddress() == RadialParticle::OSC_PARTICLE_EXPLOSION_SPEED) {
        if ( m.getNumArgs() >= 1 ) {
            RadialParticle::speed = m.getArgAsFloat(0);
        }
    }
    
    if(m.getAddress() == RadialParticle::OSC_PARTICLE_EXPLOSION_DENSITY) {
        if ( m.getNumArgs() >= 1 ) {
            RadialParticle::density = m.getArgAsFloat(0);
        }
    }
}
