#pragma once

#include "ofMain.h"
#include "Sounds.h"

#define QTY_UNITS 6
#define QTY_STEPS 16

class SequenceModeTrack {
    public:
        static Sounds * sounds;

        SequenceModeTrack();

        void toggleSound( Sound * sound, bool doProcessSequence = true );

        void processSequence();
        void clearSequence();

        void draw( bool notSelected = false );
        void onTempo();

        void unselectAllSounds();
        void selectAllSounds();

        void onNeedToProcess(int & a);

        void reset();

        bool playing = true;
        float volume = 1.0f;
        ofParameter<int> selectedUnit { "Bars", 2, 0, QTY_UNITS }; // = 2;

        int lastStep = -1;
        int currentStep = -1;

        ofParameter<int> offset {"Offset",0,0,15};
//        int offset;
        float probability = 1;

        vector<Sound*> secuencia;
    private:
        Sound ** secuenciaEnTiempo = NULL;
        static int intUnits[QTY_UNITS];

        int getCantSteps();
        void processSequenceOffset();

        ofPolyline polyLine;
        bool isProcessing = false;
};
