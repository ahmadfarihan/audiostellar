#include "Modes.h"


Modes::Modes(MidiServer *_midiServer) {
    sounds = Sounds::getInstance();
    midiServer = _midiServer;

    initModes();
}

void Modes::beforeDraw() {
    for(unsigned int i = 0; i < modes.size(); i++) {
        modes[i]->beforeDraw();
    }
}
void Modes::draw() {
    for(unsigned int i = 0; i < modes.size(); i++) {
        modes[i]->draw();
    }
}

void Modes::update() {
    for(unsigned int i = 0; i < modes.size(); i++) {
        modes[i]->update();
    }
}

void Modes::drawModeSelector(){


   ImGuiWindowFlags modeSelectorWindowFlags = 0;
   modeSelectorWindowFlags |= ImGuiWindowFlags_NoTitleBar;
   modeSelectorWindowFlags |= ImGuiWindowFlags_NoMove;
   modeSelectorWindowFlags |= ImGuiWindowFlags_NoResize;
   modeSelectorWindowFlags |= ImGuiWindowFlags_NoCollapse;
   modeSelectorWindowFlags |= ImGuiWindowFlags_AlwaysAutoResize;

   //    style->Colors[ImGuiCol_WindowBg] = ImVec4(1.0f, 0.05f, 0.07f, 1.00f);
   //El - 85 viene de haber hecho getWindowSize una vez y anotarlo :/,
   //ya que no podes hacer algo tipo getNextWindowSize
   ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - 85 ,20));
   ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.1f, 0.1f, 0.1f, 1.00f));
   ImGui::PushStyleColor(ImGuiCol_Border, ImVec4(0.0f, 0.0f, 0.0f, 0.00f));

   ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.0f, 0.0f, 0.0f, 0.00f));
   ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.0f, 0.0f, 0.0f, 0.00f));
   ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.0f, 0.0f, 0.0f, 0.00f));
   ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0,0));
   ImGui::PushStyleVar(ImGuiStyleVar_ItemInnerSpacing, ImVec2(0,0));


   //IMG BUTTONS DE MODOS
   if(ImGui::Begin("ModeSelector", NULL, modeSelectorWindowFlags)){
       isModeSelectorHovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem
                                                      | ImGuiHoveredFlags_AllowWhenBlockedByPopup
                                                      | ImGuiHoveredFlags_ChildWindows);
       
        for(unsigned int i = 0; i < modesIcons.size(); i++){

            ImVec4 tint( 0.16f, 0.29f, 0.47f, 1.0f );

            if ( atMode == modes[i] ) {
                tint = ImVec4( 61.0/255.0, 133.0/255.0, 224.0/255.0, 1.0f );
            }

            if(ImGui::ImageButton( (ImTextureID)(uintptr_t)modesIcons[i],
                                   ImVec2(40, 40),
                                   ImVec2(0, 0),
                                   ImVec2(1, 1),
                                   -1,
                                   ImVec4(0,0,0,0),
                                   tint
                                   )) {
                atMode = modes[i];
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(modeTooltips[i]);
            if ( i < modesIcons.size() - 1 ) {
                ImGui::SameLine(0,0);
            }
        }
   }
   ImGui::PopStyleColor(5);
   ImGui::PopStyleVar(2);
   ImGui::End();

}

void Modes::drawModesSettings(){
   for(int i = 0; i < modes.size(); i++){
       modes[i]->drawGui();
   }
}

GLuint Modes::loadModeIcon(const string & filePath){
   return gui.loadImage(filePath);
}

void Modes::initModes() {
    modes.push_back(new ExplorerMode(sounds));
    modes.push_back(new ParticleMode(sounds, midiServer));
    modes.push_back(new SequenceMode(sounds, midiServer));
    
    modeTooltips.push_back(Tooltip::MODE_EXPLORER);
    modeTooltips.push_back(Tooltip::MODE_PARTICLE);
    modeTooltips.push_back(Tooltip::MODE_SEQUENCE);

    for(unsigned int i = 0; i < modes.size(); i++) {
        modeNames.push_back(modes[i]->getModeName());
        modesIcons.push_back(loadModeIcon(modes[i]->iconPath));
    }

    //seleccioná un modo inicial
    atMode = modes[0];
}

void Modes::mousePressed(int x, int y, int button) {
    atMode->mousePressed( ofVec2f(x,y), button );
}

void Modes::keyPressed(ofKeyEventArgs & e) {
    atMode->keyPressed(e);
}

void Modes::mouseDragged(int x, int y, int button){
    atMode->mouseDragged(ofVec2f(x,y), button);
}

void Modes::mouseReleased(int x, int y , int button) {
    atMode->mouseReleased(ofVec2f(x,y));
}

void Modes::mouseMoved(int x, int y) {
    atMode->mouseMoved(ofVec2f(x,y));
}
void Modes::midiMessage(Utils::midiMsg m) {


    //Si está en midiLearn sólo le manda mensaje al modo activo
    if(MidiServer::midiLearn){
        atMode->midiMessage(m);
    }else{
        for(unsigned int i = 0; i < modes.size(); i++) {
                modes[i]->midiMessage(m);
        }
    }
}

Mode *Modes::getActiveMode()
{
    return atMode;
}

string Modes::getActiveModeName(){
    return atMode->getModeName();

}

bool Modes::isMouseHoveringGui() {
    bool hovered = false;
    for(int i = 0; i < modes.size(); i++) {
        hovered = hovered || modes[i]->isMouseHoveringGui();
    }
    return hovered || isModeSelectorHovered;
}
