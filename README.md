# AudioStellar - http://audiostellar.xyz

Open source data-driven musical instrument for latent sound structure discovery and music experimentation

![screenshot](http://leandrogarber.info/proyectos/AudioStellar/audiostellar_screenshot_3.jpg)

Visualize a collection of short audio samples in an interactive 2D point map which enables to analyze
resulting groups and play given samples in a novel way using various innovative music composition modes.


## Downloads

* [Linux AppImage v0.10.0](http://audiostellar.xyz/downloads/AudioStellar-0.10.0-x86_64.AppImage)
* [MacOS 64 bits v0.10.0](http://audiostellar.xyz/downloads/AudioStellar_v0.10.0.dmg)
* [Windows 64 bits v0.10.0](http://audiostellar.xyz/downloads/AudioStellar-0.10-win64.zip)
* [more](http://audiostellar.xyz)

## Machine learning pipeline

![Pipeline](https://gitlab.com/ayrsd/audiostellar/raw/master/data-analysis/proceso.png)

## Support

audiostellar [at] googlegroups.com

[Subscribe](https://groups.google.com/group/audiostellar/subscribe) to our low traffic mailing list to get updates on new versions

## Contribute

### How?

#### I'm a musician
* Make music with it
* Make your own sound map
* Give us feedback

#### I'm a programmer
* Fork it
* Hack it
* Browse our issues
* Open new issues
* Make it your own
* We love pull requests

### License

GNU/GPL v3

### How to compile

1. Download [openFrameworks](https://openframeworks.cc/) 0.11.x.
	* Linux users: use make or QtCreator
	* Mac users: use XCode project provided
	* Win users: use ~~msys2~~ Visual Studio project provided

2. Follow [openFramework's install instructions](https://openframeworks.cc/download/) and compile an example.
3. Place this project in apps/myApps. (advanced users can set OF_ROOT enviroment variable to point where you want instead)
4. From a terminal run install_addons.sh (for Windows use the git shell). This will download all the addons needed.
5. Compile.

#### Finally

At this point AudioStellar should compile and execute properly but you won't be able to create your own sound map. For achieving that :

6. Follow [data-analysis README instructions](https://gitlab.com/ayrsd/audiostellar/tree/master/data-analysis) for compiling python's machine learning process.

## OSC Support

Check out [OSC API documentation](https://gitlab.com/ayrsd/audiostellar/-/blob/master/OSC_Documentation.md)

Check out our [osc-examples](https://gitlab.com/ayrsd/audiostellar/tree/master/osc_examples) .
We are providing examples for:
* Python
* Puredata
* Max
* Touch OSC
