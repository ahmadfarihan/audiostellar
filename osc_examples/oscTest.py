#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 18 19:05:09 2018

@author: leandro
"""

#%%

from pythonosc.osc_server import BlockingOSCUDPServer
from pythonosc.udp_client import SimpleUDPClient

from pythonosc import udp_client
from pythonosc.dispatcher import Dispatcher
from pythonosc import osc_server

from typing import List, Any
import threading
import random

OSC_PLAY = "/sounds/play"
OSC_PLAY_X = "/sounds/playX"
OSC_PLAY_Y = "/sounds/playY"
OSC_PLAY_ID = "/sounds/playID"
OSC_PLAY_CLUSTER = "/sounds/playCluster"
OSC_PLAY_CLUSTER_BY_SOUNDID = "/sounds/playClusterBySoundId"

OSC_PARTICLES_EMIT = "/modes/particle/emit"
OSC_PARTICLES_VELOCITY = "/modes/particle/velocity"

OSC_SEQUENCE_VOLUME = "/sequencemode/vol"

OSC_GET_NUM_SOUNDS = "/sounds/getNumSounds"
OSC_GET_POSITION_ID = "/sounds/getPositionByID"
OSC_GET_NEIGHBORS_ID = "/sounds/getNeighborsByID"
OSC_GET_NEIGHBORS_XY = "/sounds/getNeighborsByXY"

numSounds = 0
neighbors = []
#%%

def callback(address, *args):    
    if address == OSC_GET_NUM_SOUNDS:
        numSounds = args[0]
    elif address == OSC_GET_NEIGHBORS_ID:
        global neighbors
        
        neighbors.clear();
        for a in args:
            neighbors.append(a)
        if len(neighbors) == 0:
            print("warning: no neighbors found")
    
    print(f"Address: {address}")
    for a in args:
        print(a, end=", ")
    print("")

#%%

client = SimpleUDPClient("127.0.0.1", 8000) 

dispatcher = Dispatcher()
dispatcher.set_default_handler(callback)

server = BlockingOSCUDPServer(("127.0.0.1", 9000), dispatcher)
 
# run following line is Address already in user
# server.server_close()

#%%

client.send_message(OSC_PLAY, [0.225, 0.395])

#%%

#above is the same as

client.send_message(OSC_PLAY_X, 0.225)
client.send_message(OSC_PLAY_Y, 0.395)

#%%

client.send_message(OSC_GET_NUM_SOUNDS, "")
server.handle_request()

#%%

client.send_message(OSC_PLAY_ID, 240)

#%%

# You can play previously named cluster
client.send_message(OSC_PLAY_CLUSTER, "Kicks")
# Select which sound from cluster
client.send_message(OSC_PLAY_CLUSTER, ["Kicks", 0])
client.send_message(OSC_PLAY_CLUSTER, ["Kicks", 4, 0.9])


client.send_message(OSC_PLAY_CLUSTER, ["Claps", 0])

#%%

client.send_message(OSC_GET_POSITION_ID, 240)
server.handle_request()  

#%%

client.send_message(OSC_PARTICLES_VELOCITY, [-0.5,-0.1])

for i in range(50):
    client.send_message(OSC_PARTICLES_EMIT, [ random.uniform(0.4,0.5),random.uniform(0.4,0.5)])
#%%

client.send_message(f"{OSC_SEQUENCE_VOLUME}/1", 0.5)
    
#%%
# playing around, 

def beat():
    global timer
    client.send_message(OSC_PLAY_CLUSTER, "Kicks")
    timer = threading.Timer(1.0, beat) 
    timer.start() 
  
timer = threading.Timer(1.0, beat) 
timer.start() 

#%%
    
timer.cancel()

#%%

# playing around, more complex rythm

def beat():
    global timer
    
    if random.random() > 0.5:
        client.send_message(OSC_PLAY_CLUSTER, "Snares")
    
    timer = threading.Timer(60/120/4, beat)
    timer.start() 
  
timer = threading.Timer(60/120/4, beat) 
timer.start() 

#%%

client.send_message( OSC_GET_NEIGHBORS_ID, [240, 0.01] )
server.handle_request()
#neighbors

#%%

client.send_message( OSC_GET_NEIGHBORS_XY, [0.707, 0.725, 0.01] )
server.handle_request()
#neighbors

#%%

def beat():
    global timer
    
    if len(neighbors) == 0:
        print("end")
        return
    
    if random.random() > 0.5:
        randomID = neighbors[ random.randrange(0,len(neighbors)) ]
        client.send_message(OSC_PLAY_ID, randomID)
        
        client.send_message(OSC_GET_NEIGHBORS_ID, [randomID, 0.1])
        server.handle_request()  
    
    timer = threading.Timer(60/120/4, beat)
    timer.start() 

client.send_message(OSC_GET_NEIGHBORS_ID, [5, 0.1])
server.handle_request()  
timer = threading.Timer(60/120/4, beat) 
timer.start() 

#%%

